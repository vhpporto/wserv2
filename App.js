import React, {Component} from 'react';
import { View, Text } from 'react-native'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome'
import Agendamentos from './src/components/Agendamentos'
import Servicos from './src/components/Servicos'
import Estoque from './src/components/Estoque'
import Login from './src/components/Login'
import AgendamentoStatus from './src/components/AgendamentoStatus'
import Autentica from './src/components/Autentica'



const RootStack = createStackNavigator(
    {
        Login: Login,
     },
    {
        initialRouteName: 'Login'
    },
    
{   
    
}
)


const TabNavigator = createMaterialBottomTabNavigator(  
  {  
      Agendamentos: { screen: Agendamentos,  
          navigationOptions:{   
              tabBarIcon: ({ tintColor }) => (  
                  <View>  
                      <Icon style={[{color: tintColor}]} size={25} name={'calendar'}/>  
                  </View>),  

        barStyle: { backgroundColor: '#131313' },
        activeColor: '#1e90ff',
        labeled: false
          }  
      },  
      Profile: { screen: Servicos,  
          navigationOptions:{   
              tabBarIcon: ({ tintColor }) => (  
                  <View>  
                      <Icon style={[{color: tintColor}]} size={25} name={'cut'}/>  
                  </View>),  

              barStyle: { backgroundColor: '#131313' },
              activeColor: '#1e90ff',
              labeled: false
          }  
      },  
      Image: { screen: Estoque,  
          navigationOptions:{  
              tabBarIcon: ({ tintColor }) => (  
                  <View>  
                      <Icon style={[{color: tintColor}]} size={25} name={'shopping-cart'}/>  
                  </View>),  
              barStyle: { backgroundColor: '#131313' },
              activeColor: '#1e90ff',
              labeled: false  
          }  
      }, 
      Login: { screen: Login,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'user'}/>  
                </View>),  
            barStyle: { backgroundColor: '#131313' },
            activeColor: '#1e90ff',
            labeled: false  
        }

      },
      AgendamentoStatus: { screen: AgendamentoStatus,  
        navigationOptions:{   
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'calendar'}/>  
                </View>),  

      barStyle: { backgroundColor: '#131313' },
      activeColor: '#1e90ff',
      labeled: false
        }  
    },
    Autentica: { screen: Autentica,  
        navigationOptions:{   
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'user'}/>  
                </View>),  

      barStyle: { backgroundColor: '#131313' },
      activeColor: '#1e90ff',
      labeled: false
        }  
    }, 
  })





export default createAppContainer(TabNavigator, RootStack);


