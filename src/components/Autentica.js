
import React, {Component} from 'react';
import {Alert, Button, Platform, StyleSheet, Text, View, TextInput,TouchableOpacity} from 'react-native';
import md5 from 'md5';



export default class Autentica extends Component {

  constructor() {
    super()
    this.state = {
        email: '',
        senha: ''
    }

  }
  updateValue(text, field)
  {
      if(field=='senha') 
      {
          this.setState({
              senha:text,
          })
      }
      else if (field=='email')
      {
          this.setState({
              email:text,
          })
      }
  }

  submit()
{
    let collection={}
    collection.senha=this.state.senha,
    collection.email=this.state.email
    console.warn(collection)
}
  render() {
    return (
      <View style={styles.container}>
          <TextInput style={styles.input}
          placeholder='Email'
          autoCapitalize='none'
          onChangeText={(text) => this.updateValue(text, 'email')} />
          <TextInput style={styles.input}
          placeholder='Senha'
          onChangeText={(text) => this.updateValue(text, 'senha')} />

        <TouchableOpacity style={styles.button} onPress={this._postData}>
          <Text style={styles.textButtom}>Entrar</Text>
        </TouchableOpacity>
        {/* <Text style={styles.welcome} >{this.state.text}</Text> */}
      </View>
    ) 
  }


  _postData = async () => {
    let formData = new FormData()
      formData.append('api_key', 'TcskrU3Ejze.6')
      formData.append('email', this.state.email)
      formData.append('senha', md5(this.state.senha))
      formData.append('pescodigo', '882438')
    fetch('https://api.appbarber.com.br/loginv2',{
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) =>  {
        this.setState({text: JSON.stringify(responseJson)})
      
      this.state.text[20] == 0
      ? this.props.navigation.navigate('Agendamentos')
      : Alert.alert('Alerta','Email ou senha inválidos')
      
       
    })

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  welcome: {
    fontSize: 14,
    textAlign: 'center',
    margin: 10,
    color: '#f0f0f0'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
      height: 40,
      width: '85%',
      backgroundColor: 'rgba(255,255,255,0.6)',
      borderRadius: 8,
      marginBottom: 10,
      padding: 10
      
  },
  button:{
    height: 45,
    width: '85%',
    backgroundColor: '#d55',
    paddingHorizontal: 20,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8
  },
  textButtom: {
    color: '#f0f0f0',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center'
}
});
