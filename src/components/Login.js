import React, {Component} from 'react'
import { AlertIOS, KeyboardAvoidingView, View, Text, TextInput, TouchableOpacity, StyleSheet  } from 'react-native'


export default class Login extends Component {

    
    state = {
        email: '',
        password: '',
        isAuthenticated: false,
      }





    render(){
        return(
            <KeyboardAvoidingView behavior='padding' enabled style={styles.container}>
                <TextInput style={styles.inputEmail}
                        placeholder="Digite seu e-mail"
                        keyboardType='email-address'
                        autoCapitalize='none'
                        autoCorrect={false} />
                <TextInput style={styles.inputSenha}
                        placeholder="Digite sua senha"
                        autoCapitalize='none'
                        autoCorrect={false} 
                        secureTextEntry={true}
                         />
                <TouchableOpacity style={styles.button}
                onPress={() => this.props.navigation.navigate('Agendamentos')}>
                        <Text>Logar</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000'
    },
    inputEmail: {
        height: 45,
        width: '85%',
        backgroundColor: 'rgba(255,255,255,0.6)',
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 20,
        marginTop: 220,
        borderRadius: 8
    },
    inputSenha: {
        height: 45,
        width: '85%',
        backgroundColor: 'rgba(255,255,255,0.6)',
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 20,
        borderRadius: 8
    },
    button:{
        height: 45,
        width: '85%',
        backgroundColor: '#d55',
        paddingHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
      },
      textButtom: {
          color: '#f0f0f0',
          fontWeight: 'bold',
          justifyContent: 'center',
          alignItems: 'center'
      }

})