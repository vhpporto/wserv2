import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ActivityIndicator, SafeAreaView, ScrollView,StatusBar} from 'react-native';

export default class App2 extends Component {

    constructor(props) {
      super(props)
      this.state =  {
        isLoading: true,
        dataSource: null,
      }
    }
  
      componentDidMount () {
        return fetch('https://ws.appbeleza.com.br/Service.php?metodo=buscaServicos&id=d3121a7d3f18480990a3fcf34d36538f&tipo=1&dia=2')
  
        
        .then((response) => response.json() )  
          .then ((responseJson) => {
  
            this.setState({
              isLoading: false,
              dataSource: responseJson.servicos, //nome obj json
            })
          })
  
          .catch((error) => {
            console.log(console.error)
  
          })
      }
  
    render() {
  
      if(this.state.isLoading) {
  
        return (
          <View style={styles.container}>
            <ActivityIndicator />
            </View>
            
        )
      } else {
  
        // let movies = this.state.dataSource.map((val,key) =>  {
        //   return <View key={key} style={styles.item}>
        //         <Text> Servico: {val.Descricao}</Text>
        //         <Text> {val.Age_Dat_Inicio}</Text>
        //         </View>
  
          let buscaServicos = this.state.dataSource.map((val,key) =>  {
          return <View style={styles.container}key={key}>
                <Text style={styles.agendamentos}> Descrição: {val.Ser_Descricao} {'\n'}
                Valor: {val.Ser_Valor} {'\n'}
                </Text>
            
                </View>
  
  
        })
          return (
  
  
        <ScrollView style={styles.welcome}>
              <StatusBar
              backgroundColor="blue"
              barStyle="light-content"/>
          <Text style={styles.welcome}>Serviços</Text>
         {buscaServicos}
  
  
        </ScrollView>
      )
      }
    }
  }


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000',
    },
    welcome: {
      fontSize: 25,
      textAlign: 'center',
      fontWeight: 'bold',
      color: 'lightgray',
      paddingVertical: 15,
      // margin: 15,
      backgroundColor: 'black',
    },
    agendamentos: {
      width: '80%',
      height: 70,
      fontSize: 15,
      fontWeight: 'bold',
      textAlign: 'center',
      alignItems: 'center',
      marginBottom: 30,
      color: 'lightgray',
      justifyContent: 'center',
      borderColor: '#f0f0f0',
      borderWidth: 1,
      backgroundColor: '#393939',
      borderRadius: 8,
      overflow: 'hidden'
  
    },
    linha: {
      flex: 1,
      paddingRight: 15,
      marginTop: 8,
      paddingTop: 13,
      paddingBottom: 13,
      // borderBottomWidth: 0.5,
      // borderColor: 'gray',
      // borderRadius: 3,
      flexDirection: 'row',
      alignItems: 'center',
      color: 'lightgray',
      borderWidth: StyleSheet.hairlineWidth,
      margin: StyleSheet.hairlineWidth,
      // backgroundColor: '#333c4c'
    },
    list:{
      borderRadius: 0.8,
      borderColor: '#f0f0f0'
    },
    data: {
      fontSize: 15,
      textAlign: 'center',
      fontWeight: 'bold',
      color: 'lightgray',
      justifyContent: 'flex-start',
      marginBottom: 10,
      backgroundColor: 'black',
    },
    scrollView: {
      // backgroundColor: 'pink',
      //marginVertical: 10,
      marginHorizontal: 3,
      textAlign: 'center',
    },
    item: {
      flex: 1,
      flexDirection: 'column',
      margin: 3,
      borderColor: '#222',
      borderWidth: 3,
      borderRadius: 10,
      borderColor: '#449'
    },
  
  
  });
  
  